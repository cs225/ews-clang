FROM centos:7

ARG LLVM_VERSION=3.9.1
ARG PREFIX=/software/llvm-${LLVM_VERSION}

RUN yum install -y epel-release gcc gcc-c++ python-devel ncurses ncurses-devel libxml2-devel swig libedit-devel patch && yum install -y ninja-build

RUN mkdir -p /build/downloads; \
    for d in llvm cfe compiler-rt libcxx libcxxabi lldb lld libunwind; do \
        url=http://releases.llvm.org/${LLVM_VERSION}/$d-${LLVM_VERSION}.src.tar.xz; \
        echo $url; \
        curl -o /build/downloads/$d.src.tar.xz $url; \
    done \
    && curl -o /tmp/cmake.src.tar.gz https://cmake.org/files/v3.7/cmake-3.7.2-Linux-x86_64.tar.gz

RUN mkdir -p /build/src/llvm /build/src/llvm/tools/{clang,lld} /build/src/llvm/projects/{compiler-rt,libcxx,libcxxabi,lldb,libunwind} /tmp/cmake \
    && tar xJ --strip-components=1 -C /build/src/llvm                      -f /build/downloads/llvm.src.tar.xz        \
    && tar xJ --strip-components=1 -C /build/src/llvm/tools/clang          -f /build/downloads/cfe.src.tar.xz         \
    && tar xJ --strip-components=1 -C /build/src/llvm/tools/lld            -f /build/downloads/lld.src.tar.xz         \
    && tar xJ --strip-components=1 -C /build/src/llvm/projects/compiler-rt -f /build/downloads/compiler-rt.src.tar.xz \
    && tar xJ --strip-components=1 -C /build/src/llvm/projects/libcxx      -f /build/downloads/libcxx.src.tar.xz      \
    && tar xJ --strip-components=1 -C /build/src/llvm/projects/libcxxabi   -f /build/downloads/libcxxabi.src.tar.xz   \
    && tar xJ --strip-components=1 -C /build/src/llvm/projects/libunwind   -f /build/downloads/libunwind.src.tar.xz   \
    && tar xz --strip-components=1 -C /tmp/cmake   -f /tmp/cmake.src.tar.gz

COPY ["clang-rpath.patch", "clang-default-ld.patch", "clang-default-rt.patch", "/tmp/patches/"]
RUN cd /build/src/llvm/tools/clang \
    && patch -p1 < /tmp/patches/clang-rpath.patch \
    && patch -p1 < /tmp/patches/clang-default-rt.patch \
    && patch -p1 < /tmp/patches/clang-default-ld.patch

RUN mkdir /build/build.1 \
    && cd /build/build.1 \
    && /tmp/cmake/bin/cmake -G Ninja \
            -DCMAKE_BUILD_TYPE=Release \
            -DCMAKE_INSTALL_PREFIX=${PREFIX} \
            -DCMAKE_C_FLAGS="-I${PREFIX}/include" \
            -DCMAKE_CXX_FLAGS="-I${PREFIX}/include" \
            -DCLANG_DEFAULT_CXX_STDLIB="libc++" \
            -DCLANG_DEFAULT_LINKER="lld" \
            -DCLANG_DEFAULT_RTLIB="compiler-rt" \
            -DLIBCXX_CXX_ABI="libcxxabi" \
            -DLIBCXX_CXX_ABI_INCLUDE_PATHS="/build/src/llvm/projects/libcxxabi/include/" \
            -DLIBCXX_CXX_ABI_LIBRARY_PATH="${PREFIX}/lib" \
            -DLIBCXXABI_USE_LLVM_UNWINDER=on \
            -DLIBUNWIND_ENABLE_SHARED=0 \
            -DLLVM_ENABLE_ASSERTIONS=off \
            -DLIBCXX_HAS_GCC_S_LIB=0 \
            /build/src/llvm

RUN cd /build/build.1 \
    && ninja-build cxxabi \
    && ninja-build install-libcxxabi \
    && ninja-build

RUN cd /build/build.1 \
    && ninja-build install \
    && cd /build \
    && rm -rf /build/build.1

# RUN tar xJv --strip-components=1 -C /build/src/llvm/projects/lldb        -f /build/downloads/lldb.src.tar.xz

# TODO: move this into the build above
RUN sed -i 's/-lc++abi/& -lunwind/' $PREFIX/lib/libc++.so

RUN mkdir /build/build.2 \
    && cd /build/build.2 \
    && /tmp/cmake/bin/cmake -G Ninja \
            -DCMAKE_BUILD_TYPE=Release \
            -DCMAKE_INSTALL_PREFIX=${PREFIX} \
            -DCMAKE_C_COMPILER=${PREFIX}/bin/clang \
            -DCMAKE_CXX_COMPILER=${PREFIX}/bin/clang++ \
            -DCMAKE_C_FLAGS="-I${PREFIX}/include" \
            -DCMAKE_CXX_FLAGS="-I${PREFIX}/include" \
            -DCLANG_DEFAULT_CXX_STDLIB="libc++" \
            -DCLANG_DEFAULT_LINKER="lld" \
            -DCLANG_DEFAULT_RTLIB="compiler-rt" \
            -DLIBCXX_CXX_ABI="libcxxabi" \
            -DLIBCXX_CXX_ABI_INCLUDE_PATHS="/build/src/llvm/projects/libcxxabi/include/" \
            -DLIBCXXABI_USE_COMPILER_RT=on \
            -DLIBCXXABI_USE_LLVM_UNWINDER=on \
            -DLIBUNWIND_ENABLE_SHARED=0 \
            -DLIBCXX_HAS_GCC_S_LIB=0 \
            -DLLVM_ENABLE_ASSERTIONS=off \
            /build/src/llvm

RUN cd /build/build.2 \
    && ninja-build cxxabi cxx \
    && sed -i 's/-lc++abi/& -lunwind/' lib/libc++.so \
    && ninja-build

RUN cd /build/build.2 \
    # && sed -i '/INSTALL DESTINATION/ s/.*lib\>/&64/' projects/lldb/scripts/cmake_install.cmake \
    && ninja-build install \
    && cd /build \
    && rm -rf /build/build.2
